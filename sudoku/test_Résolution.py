import os
os.chdir('C:\\Users\\nicop\\PycharmProjects\\sudoku_propro\\sudoku')

from Résolution import *

Sudoku = [[7, 0, 5, 0, 6, 8, 0, 0, 2],
          [4, 0, 0, 0, 0, 0, 6, 7, 0],
          [2, 0, 0, 5, 0, 0, 0, 0, 0],
          [0, 1, 0, 0, 7, 4, 0, 2, 0],
          [0, 0, 0, 0, 0, 0, 0, 0, 0],
          [0, 2, 0, 1, 5, 0, 0, 4, 0],
          [0, 0, 0, 0, 0, 9, 0, 0, 7],
          [0, 9, 3, 0, 0, 0, 0, 0, 4],
          [5, 0, 0, 4, 2, 0, 3, 0, 9]]

Sudoku_plein = [[7, 3, 5, 9, 6, 8, 4, 1, 2],
                [4, 8, 9, 2, 1, 3, 6, 7, 5],
                [2, 6, 1, 5, 4, 7, 8, 9, 3],
                [9, 1, 8, 3, 7, 4, 5, 2, 6],
                [6, 5, 4, 8, 9, 2, 7, 3, 1],
                [3, 2, 7, 1, 5, 6, 9, 4, 8],
                [8, 4, 2, 6, 3, 9, 1, 5, 7],
                [1, 9, 3, 7, 8, 5, 2, 6, 4],
                [5, 7, 6, 4, 2, 1, 3, 8, 9]]

Sudoku_faux = [[3, 3, 5, 9, 6, 8, 4, 1, 2],
                [4, 8, 9, 2, 1, 3, 6, 7, 5],
                [2, 6, 1, 5, 4, 7, 8, 9, 3],
                [9, 1, 8, 3, 7, 4, 5, 2, 6],
                [6, 5, 4, 8, 9, 2, 7, 3, 1],
                [3, 2, 7, 1, 5, 6, 9, 4, 8],
                [8, 4, 2, 6, 3, 9, 1, 5, 7],
                [1, 9, 3, 7, 8, 5, 2, 6, 4],
                [5, 7, 6, 4, 2, 1, 3, 8, 9]]


def test_ligne():
    assert ligne(Sudoku, 2) == [2, 0, 0, 5, 0, 0, 0, 0, 0]


def test_colonne():
    assert colonne(Sudoku, 4) == [6, 0, 0, 7, 0, 5, 0, 0, 2]


def test_num_carre():
    assert num_carre(1, 3) == 4


def test_carre():
    assert carre(Sudoku, 1, 3) == [0, 1, 0, 0, 0, 0, 0, 2, 0]


def test_possible():
    assert possible(Sudoku, 0, 3) == [3, 6, 8, 9]
    assert possible(Sudoku, 0, 0) == [7]


def test_cases_vides():
    assert cases_vides(Sudoku) == True
    assert cases_vides(Sudoku_plein) == False

def test_resolu():
    assert resolu(Sudoku) == False
    assert resolu(Sudoku_plein) == True
    assert resolu(Sudoku_faux) == False
