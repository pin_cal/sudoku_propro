Sudoku_moyen = [[7, 0, 5, 0, 6, 8, 0, 0, 2],
                [4, 0, 0, 0, 0, 0, 6, 7, 0],
                [2, 0, 0, 5, 0, 0, 0, 0, 0],
                [0, 1, 0, 0, 7, 4, 0, 2, 0],
                [0, 0, 0, 0, 0, 0, 0, 0, 0],
                [0, 2, 0, 1, 5, 0, 0, 4, 0],
                [0, 0, 0, 0, 0, 9, 0, 0, 7],
                [0, 9, 3, 0, 0, 0, 0, 0, 4],
                [5, 0, 0, 4, 2, 0, 3, 0, 9]]

Sudoku_difficile = [[0, 5, 6, 0, 0, 0, 3, 0, 0],
                    [0, 0, 0, 0, 0, 0, 0, 5, 8],
                    [0, 0, 0, 0, 4, 9, 6, 0, 0],
                    [5, 9, 0, 6, 2, 4, 0, 0, 7],
                    [0, 0, 0, 0, 0, 0, 0, 0, 0],
                    [2, 0, 0, 9, 8, 1, 0, 6, 4],
                    [0, 0, 5, 2, 3, 0, 0, 0, 0],
                    [1, 6, 0, 0, 0, 0, 0, 0, 0],
                    [0, 0, 8, 0, 0, 0, 7, 9, 0]]

Sudoku_diabolique = [[9, 0, 0, 0, 7, 8, 0, 0, 6],
                     [0, 5, 8, 3, 0, 0, 0, 0, 0],
                     [0, 0, 0, 0, 4, 0, 7, 0, 0],
                     [0, 0, 1, 8, 0, 9, 0, 0, 2],
                     [0, 0, 9, 0, 0, 0, 8, 0, 0],
                     [3, 0, 0, 7, 0, 4, 5, 0, 0],
                     [0, 0, 4, 0, 8, 0, 0, 0, 0],
                     [0, 0, 0, 0, 0, 7, 2, 1, 0],
                     [2, 0, 0, 6, 5, 0, 0, 0, 9]]

Sudoku_demoniaque = [[0, 5, 0, 0, 7, 0, 0, 6, 0],
                     [7, 0, 8, 0, 0, 0, 9, 0, 3],
                     [0, 1, 2, 0, 0, 0, 4, 7, 0],
                     [0, 0, 0, 6, 3, 7, 0, 0, 0],
                     [0, 0, 7, 0, 0, 0, 3, 0, 0],
                     [0, 0, 0, 2, 9, 1, 0, 0, 0],
                     [0, 9, 4, 0, 0, 0, 7, 2, 0],
                     [2, 0, 5, 0, 0, 0, 8, 0, 1],
                     [0, 7, 0, 0, 4, 0, 0, 3, 0]]


def ligne(S, i):  #Renvoie toute la i-ème ligne, i de 0 à 8#
    return S[i]


def colonne(S, j):   #Renvoie toute la j-ème ligne, j de 0 à 8#
    colonne = []
    for i in range(0, 9):
        colonne.append(S[i][j])
    return colonne


def num_carre(i, j):
    #Renvoie le numéro du carré dans lequel se trouve la case (i,j) de 0 à 8#
    return 3*(i//3)+(j//3)


def carre(S, i, j):
    #Renvoie tout le carré contenant la case (i,j)#
    n = num_carre(i, j)
    l1 = S[3 * (n // 3)][3 * (n % 3): 3 * (n % 3) + 3]
    l2 = S[3 * (n // 3) + 1][3 * (n % 3): 3 * (n % 3) + 3]
    l3 = S[3 * (n // 3) + 2][3 * (n % 3): 3 * (n % 3) + 3]
    return l1 + l2 + l3


def possible(S, i, j):
    #Renvoie la liste des valeurs possibles pour la case (i, j)
    if S[i][j] != 0:
        return [S[i][j]]

    L = ligne(S, i)
    K = colonne(S, j)
    C = carre(S, i, j)

    impossible = []
    possible = []

    for x in L:
        impossible.append(x) # x existe déjà dans la ligne
    for x in K:
        impossible.append(x) # x existe déjà dans la colonne
    for x in C:
        impossible.append(x) # x existe déjà dans le carré

    for i in range(1, 10):
        if i not in impossible:
            possible.append(i)

    return possible


def possible_general(S):
    ### Renvoie un tableau des valeurs possibles dans chaque case du sudoku
    possible_g = [[0 for i in range(9)] for j in range(9)]

    for i in range(9):
        for j in range(9):
            possible_g[i][j] = possible(S, i, j)

    return possible_g


def cases_vides(S):
    ### Renvoie la liste des indices (i, j) des cases vides
    liste_cases_vides = []
    for i in range(9):
        for j in range(9):
            if S[i][j] == 0: # La case est vide
                liste_cases_vides.append((i, j))
    return liste_cases_vides


def resolution_moyen(S):
    ### Méthode basique de résolution
    possible_g = possible_general(S)
    S0 = [[0 for i in range(9)] for j in range(9)]
    while cases_vides(S) != []:
        for i in range(9):
            for j in range(9):
                S0[i][j] = S[i][j] # Copie de la grille
        for c in cases_vides(S): # On teste toutes les cases vides
            i, j = c[0], c[1]
            if len(possible_g[i][j]) == 0: # Aucune valeur possible dans la case (servira pour la méthode suivante)
                return 'Erreur'
            for k in possible_g[i][j]: # On teste chaque valeur possible pour la case
                n1, n2, n3 = 0, 0, 0
                for P in ligne(possible_g, i):
                    if k in P:
                        n1 += 1
                if n1 == 1: # k ne peut être qu'ici dans la ligne
                    S[i][j] = k
                    break

                for P in colonne(possible_g, j):
                    if k in P:
                        n2 += 1
                if n2 == 1: # k ne peut être qu'ici dans la colonne
                    S[i][j] = k
                    break

                for P in carre(possible_g, i, j):
                    if k in P:
                        n3 += 1
                if n3 == 1: # k ne peut être qu'ici dans le carré
                    S[i][j] = k
                    break

        if S == S0:
            break
        possible_g = possible_general(S) # On réinitialise la grille de possibilités après avoir fait les modifications

    return S

def resolution_hypothese(S):
    ### Méthode utilisant des hypothèses pour résoudre
    S = resolution_moyen(S)
    while cases_vides(S) != []:
        # On s'arrête quand le sudoku est rempli
        x = 9
        y = 9
        for i in range(9):
            for j in range(9) :
                if len(possible(S, i, j)) == 2:
                    # On choisit de faire des hypothèses sur une case qui n'a que deux valeurs possibles
                    x = i
                    y = j
        if x > 8 or y > 8:
            # Aucune case avec deux valeurs possibles : la fonction s'arrête
            return S
        P = possible(S, x, y)
        S0 = [[0 for i in range(9)] for j in range(9)]
        for i in range(9):
            for j in range(9):
                S0[i][j] = S[i][j] # Copie de la grille
        S0[x][y] = P[0] # Hypothèse
        S0 = resolution_moyen(S0)
        if S0 == 'Erreur': # L'hypothèse mène à une impossibilité
            S[x][y] = P[1] # Il faut donc prendre l'autre valeur
            S = resolution_moyen(S)
        else:
            S = S0 # Sinon on continue à résoudre
    return S


def resolu(S):
    # Vérifie si le sudoku est résolu

    if cases_vides(S) != []:
        # Si il reste des cases vides ce n'est pas résolu
        return False

    resolu = [1, 2, 3, 4, 5, 6, 7, 8, 9]
    ### On vérifie que chaque ligne, colonne et carré contient les nombres de 1 à 9
    for i in range(9):
        for j in range(9):
            L = sorted(ligne(S, i))
            K = sorted(colonne(S, j))
            C = sorted(carre(S, i, j))
            if L != resolu:
                return False
            if K != resolu:
                return False
            if C != resolu:
                return False

    return True
