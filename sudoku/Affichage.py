from tkinter import *

import os
os.chdir('C:\\Users\\nicop\\PycharmProjects\\sudoku_propro\\sudoku')
from Résolution import *


def affichage_grille(S):
    # Affiche la grille S prise en entrée
    game = Tk() # Fenêtre d'affichage
    game.resizable(False, False)
    background = Frame(game, bg = 'black') # Frame principal qui va stocker les carrés
    background.grid()
    for k in range(3):
        for l in range(3):
            # On crée les carrés principaux
            carre = Frame(background, bg = 'black')
            carre.grid(row = k, column = l, padx = 1, pady = 1)
            for i in range(3):
                for j in range(3):
                    # On crée les cases dans le carré
                    tile = Frame(carre, bg = 'white', width = 50, height = 50)
                    tile.grid(row = i, column = j, padx = 1, pady = 1)
                    if S[3*k + i][3*l + j] == 0:
                        # Si la case est vide on n'affiche pas le 0
                        valeur = Label(master = tile, text = '', bg = 'white',
                                       justify = CENTER, font = 'Verdana 40', width = 2, height = 1)
                    else:
                        # Sinon on affiche la valeur de la case
                        valeur = Label(master = tile, text = str(S[3*k + i][3*l + j]), bg = 'white',
                                       justify = CENTER, font = 'Verdana 40', width = 2, height = 1)
                    valeur.grid()
    game.mainloop()


def resolution_affichage(S):
    # Résout le sudoku et affiche la grille résolue
    S0 = [[S[i][j] for i in range(9)] for j in range(9)]
    S = resolution_hypothese(S)
    game = Tk()
    background = Frame(game, bg = 'black') # Frame principal qui va stocker les carrés
    background.grid()
    for k in range(3):
        for l in range(3):
            # On crée les carrés principaux
            carre = Frame(background, bg = 'black')
            carre.grid(row = k, column = l, padx = 1, pady = 1)
            for i in range(3):
                for j in range(3):
                    # On crée les cases dans le carré
                    tile = Frame(carre, bg = 'white', width = 50, height = 50)
                    tile.grid(row = i, column = j, padx = 1, pady = 1)
                    if S0[3*k + i][3*l + j] == 0:
                        # Si la case est vide initialement on affiche le chiffre en rouge
                        valeur = Label(master = tile, text = str(S[3*k + i][3*l + j]), bg = 'white', fg = 'red',
                                       justify = CENTER, font = 'Verdana 40', width = 2, height = 1)
                    else:
                        # Si la case était donnée on l'affiche en noir
                        valeur = Label(master = tile, text = str(S[3*k + i][3*l + j]), bg = 'white',
                                       justify = CENTER, font = 'Verdana 40', width = 2, height = 1)
                    valeur.grid()
    game.mainloop()

